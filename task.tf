resource "null_resource" "command_delete_hostfile" {
  provisioner "local-exec" {
    command = "rm -f host.list"
  }
}

resource "google_compute_disk" "test_disk_8" {
  name = "test-8"
  type = "pd-standard"
  size = 8
  zone = var.gcp_zone
}

resource "google_compute_disk" "test_disk_10" {
  name = "test-10"
  type = "pd-standard"
  size = 10
  zone = var.gcp_zone
}

resource "google_compute_address" "teststaticip" {
  name = "test-static-address"
}

data "google_compute_image" "debian_image" {
  family  = "debian-10"
  project = "debian-cloud"
}

resource "google_compute_attached_disk" "test_8" {
  disk     = google_compute_disk.test_disk_8.id
  instance = google_compute_instance.instance_with_ip.id
}

resource "google_compute_attached_disk" "test_10" {
  disk     = google_compute_disk.test_disk_10.id
  instance = google_compute_instance.instance_with_ip.id
}

resource "google_compute_instance" "instance_with_ip" {
  name         = "vm-instance"
  machine_type = "e2-small"
  tags         = ["terraform-servers"]

  network_interface {
    network = "default"
    access_config {
      nat_ip = google_compute_address.teststaticip.address
    }
  }

  boot_disk {
    initialize_params {
      image = data.google_compute_image.debian_image.self_link
    }
  }

  provisioner "local-exec" {
    command = "echo ${self.name} ${google_compute_address.teststaticip.address} >> host.list"
  }

  connection {
    user = "suslov_ubuntu"
    timeout = "2m"
    host = google_compute_address.teststaticip.address
  }

  provisioner "remote-exec" {
    inline = [
      "sudo apt-get -y update",
      "sudo apt-get -y install docker.io",
      "sudo docker run -v /usr/share/nginx/html/:/usr/share/nginx/html/ -p 80:80 -d nginx:latest",
      "echo Juneway ${google_compute_address.teststaticip.address} ${self.name} | sudo tee /usr/share/nginx/html/index.html",
    ]
  }
}

resource "cloudflare_record" "juneway" {
  zone_id = var.cloudflare_zone_id
  name = "ayususlov.juneway.pro"
  value = google_compute_address.teststaticip.address
  type = "A"
  ttl = 3600
}
