resource "google_compute_firewall" "http" {
  name    = "terraform-nginx"
  network = "default"

  dynamic "allow" {
    for_each = ["80", "8001", "8802"]
    content {
      protocol = "tcp"
      ports    = [allow.value]
    }
  }

  source_ranges = ["0.0.0.0/0"]
  target_tags = ["terraform-servers"]
}
