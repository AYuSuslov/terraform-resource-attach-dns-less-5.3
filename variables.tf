variable "gcp_project" {}
variable "gcp_region" {}
variable "gcp_zone" {}
variable "gcp_creds" {}
variable "cloudflare_email" {}
variable "cloudflare_api_key" {}
variable "cloudflare_zone_id" {}
