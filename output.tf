output "Hostname" {
  value = google_compute_instance.instance_with_ip.name
}

output "GCP_Project_ID" {
  value = google_compute_instance.instance_with_ip.project
}
